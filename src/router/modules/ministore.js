import Layout from '@/layout'
import { roterPre } from '@/settings'
const ministoreRouter =
{
  path: `${roterPre}/ministore`,
  name: 'ministore',
  meta: {
    title: '视频号'
  },
  alwaysShow: true,
  component: Layout,
  redirect: 'noRedirect',
  children: [
    {
      path: 'category',
      name: 'category',
      meta: {
        title: '类目管理',
        noCache: true
      },
      component: () => import('@/views/ministore/category')
    },
    {
        path: 'product',
        name: 'product',
        meta: {
          title: '视频管理',
          noCache: true
        },
        component: () => import('@/views/ministore/product')
      },
  ]
}

export default ministoreRouter

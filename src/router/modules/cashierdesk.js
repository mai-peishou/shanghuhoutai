import Layout from '@/layout'
import {
  roterPre
} from '@/settings'
const cashierdeskRouter = {
  path: `${roterPre}/cashierdesk`,
  name: 'cashierdesk',
  meta: {
    icon: '',
    title: '收银台'
  },
  alwaysShow: true,
  component: Layout,
  children: [{
      path: 'cashierdesk',
      name: 'cashierdesk',
      meta: {
        title: '收银台'
      },
      component: () => import('@/views/cashierdesk/cashier/index')
    },
    {
      path: 'address',
      name: 'address',
      meta: {
        title: '自提收货地址'
      },
      component: () => import('@/views/cashAddress/index')
    },
  ]
}

export default cashierdeskRouter

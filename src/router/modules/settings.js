import variables from '@/styles/element-variables.scss'
import defaultSettings from '@/settings'

const { showSettings, tagsView, fixedHeader, sidebarLogo } = defaultSettings

const state = {
  // theme: variables.theme,
  theme: JSON.parse(localStorage.getItem('themeColor')) ? JSON.parse(localStorage.getItem('themeColor')) : '#1890ff',
  showSettings: showSettings,
  tagsView: tagsView,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo,
  isEdit: false,
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  },
  SET_ISEDIT: (state, isEdit) => {
    state.isEdit = isEdit
  },
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
  },
  setEdit({ commit }, isEdit) {
    commit('SET_ISEDIT', isEdit)
  }
 
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}


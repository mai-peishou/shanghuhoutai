import request from './request'

/**
 * @description 类目列表
 */
export function categoryListApi(data) {
    return request.get('ministore/category/lst', data)
}

/**
 * @description 类目列表
 * 
 */
export function categorySyncApi() {
    return request.get('ministore/category/sync', '', {timeout:20000})
}

/**
 * @description 上传资质
 * 
 */
 export function qualificationApi(data) {
    return request.post('ministore/category/qualification', data)
}


/**
 * @description 商品列表
 */
export function productListApi(data) {
    return request.get('ministore/product/lst', data)
}

/**
 * @description 商品添加
 */
 export function produCreateApi(data) {
    return request.post('ministore/product/create', data)
}

/**
 * @description 商品编辑
 */
 export function productUpdateApi(id, data) {
    return request.post('ministore/product/update' + id, data)
}

/**
 * @description 商品上架
 */
 export function listingApi(data) {
    return request.post('ministore/product/listing', data)
}

/**
 * @description 商品 下架 
 */
 export function delistingApi(data) {
    return request.post('ministore/product/delisting', data)
}

/**
 * @description 商品修改类目 
 */
 export function chanegCateApi(data) {
    return request.post('ministore/product/changeCate', data)
}

/**
 * @description 商品 删除 
 */
 export function deleteApi(data) {
    return request.post('ministore/product/delete', data)
}


/**
 * @description 同步商品
 */
 export function syncProductApi(data) {
    return request.get('ministore/product/sync', data)
}
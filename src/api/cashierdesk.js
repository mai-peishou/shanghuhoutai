import request from './request'

export function getgoods(id) {
  return request.get('store/product/cashierdetail/' + id)
}

export function productLstApi(data) {
  return request.get(`store/product/lst`, data)
}

export function categoryListApi() {
  return request.get(`store/category/lst`)
}

export function getUser(id) {
  return request.get(`user/getUser`, id)
}
export function addcart(data) {
  return request.post(`store/order/cart/create`, data)
}

export function pay(data) {
  return request.post(`store/order/pay`, data)
}

export function orderstatus(id, data) {
  return request.get(`store/order/status/` + id, data)
}
export function printer(data) {
  return request.post(`printer`, data)
}

//获取商户默认收货地址
export function mer_address(id) {
  return request.get(`store/order/mer_address`)
}
//获取用户默认收货地址
export function user_address(uid) {
  return request.get(`store/order/user_address/` + uid)
}

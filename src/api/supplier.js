import request from './request'

/**
 * @description 商户后台-供货商信息
 */
export function getsupplieriofn(id) {
    return request.get(`store/product/supplier/detail/${id}`)
  }

  /**
 * @description 商户后台-供货商添加商品
 */
export function addgoods(id) {
  return request.post(`store/product/supplier/productadd`,id)
}

/**
 * @description 商户后台-获取供货商商品分类
 */
export function getgoodscate() {
  return request.get(`store/product/supplier/productcate`)
}

/**
 * @description  商户后台-获取供货商商品列表
 */
export function getsuppliergoodslist(data) {
  return request.get(`store/product/supplier/productlist`,data)
}

/**
 * @description  商户后台-获取供货商列表
 */
export function getsupplierlist(data) {
  return request.get(`store/product/supplier/list`, data)
}
  